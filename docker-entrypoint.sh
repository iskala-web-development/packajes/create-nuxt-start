#!/bin/sh

# Install node_modules
bun install --dev

# pnpm commands
if [ $NODE_ENV = "production" ]; then bun run build; fi

exec "$@"