import { defineNuxtPlugin } from 'nuxt/app'
import app from '@/assets/ts/app'

export default defineNuxtPlugin(() => {
  return {
    provide: app
  }
})
