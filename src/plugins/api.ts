export default defineNuxtPlugin((nuxtApp) => {
  const runtimeConfig = useRuntimeConfig()

  const generateBaseUrlFull = () => {
    let baseUrlApi = runtimeConfig.public.baseUrlApi as string

    if (runtimeConfig.public.baseUrlPrefix)
      baseUrlApi += `/${runtimeConfig.public.baseUrlPrefix}`

    if (runtimeConfig.public.baseUrlVersion)
      baseUrlApi += `/${runtimeConfig.public.baseUrlVersion}`

    return baseUrlApi
  }

  const api = $fetch.create({
    baseURL: generateBaseUrlFull(),
    headers: {
      Accept: 'application/json'
    }
  })

  // Expose to useNuxtApp().$api
  return {
    provide: {
      api: {
        send: api
      }
    }
  }
})
