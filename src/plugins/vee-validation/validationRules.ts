export const password = (value: string, callback: any = () => {}) => {
  const successRules = {
    minCharacters: false,
    minUppercase: false,
    minLowercase: false,
    minNumber: false,
    minSymbol: false
  }

  if (value.length >= 8) successRules.minCharacters = true
  else successRules.minCharacters = false

  if (value.match(/^(?=.*?[A-ZА-Я]).{8,}$/g)) successRules.minUppercase = true
  else successRules.minUppercase = false

  if (value.match(/^(?=.*?[a-zа-я]).{8,}$/g)) successRules.minLowercase = true
  else successRules.minLowercase = false

  if (value.match(/^(?=.*?[0-9]).{8,}$/g)) successRules.minNumber = true
  else successRules.minNumber = false

  if (value.match(/^(?=.*?[#?!@$ %^&*-]).{8,}$/g)) successRules.minSymbol = true
  else successRules.minSymbol = false

  if (typeof callback === 'function') callback(successRules)

  if (value.length > 32) return 'Пароль не может быть больше 32 символов'

  if (
    !value.match(
      /^(?=.*?[A-ZА-Я])(?=.*?[a-zа-я])(?=.*?[0-9])(?=.*?[#?!@$ %^&*-]).{8,32}$/g
    )
  )
    return 'Пароль введен не корректно'

  return true
}

export const string = (value: string, [name]: any) => {
  if (name) {
    if (!value.match(/^[А-Яа-я]+$/))
      return 'Данное поле может содержать только русские буквы'
  } else {
    if (!value.match(/^[А-Яа-яA-Za-z]+$/))
      return 'Данное поле может содержать только буквы'
  }

  return true
}
