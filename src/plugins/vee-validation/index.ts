import { defineRule, configure } from 'vee-validate'
import { required, email, confirmed, min, max } from '@vee-validate/rules'
import { localize } from '@vee-validate/i18n'
import { password, string } from './validationRules'

export default defineNuxtPlugin(() => {
  defineRule('required', required)
  defineRule('requiredSelect', required)

  defineRule('email', email)

  defineRule('confirmed', confirmed)
  defineRule('confirmed_password', confirmed)

  defineRule('min', min)

  defineRule('max', max)

  defineRule('password', password)

  defineRule('string', string)

  configure({
    generateMessage: localize('ru', {
      messages: {
        required: 'Данное поле обязательно для заполнения',
        requiredSelect: 'Выберете значение',
        email: 'E-mail введен не корректно',
        min: 'Данное поле должно содержать не менее 0:{value} символ',
        max: 'Данное поле должно содержать не более 0:{value} символов',
        confirmed: 'Поле {field} не совпадает с полем {value}',
        confirmed_password: 'Пароли не совпадают'
      }
    })
  })

  return {
    provide: {
      validationRules: { password }
    }
  }
})
