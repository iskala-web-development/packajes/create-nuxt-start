export default () => {
  let timeoutId: any

  const timeout = async (callback: () => void, time: number = 500) => {
    clearTimeout(timeoutId)

    timeoutId = setTimeout(function () {
      callback()
    }, time)
  }

  const clear = () => clearTimeout(timeoutId)

  return { timeout, clear }
}
