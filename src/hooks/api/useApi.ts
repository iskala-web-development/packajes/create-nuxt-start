import { fetchApiErrorHandler, type Data } from './handlers/fetchApiHandler'
import { useUserStore } from '~/stores/user'

export default () => {
  const { $api } = useNuxtApp()
  const { apiToken } = storeToRefs(useUserStore())

  let params: object | object[] = {}
  let body: object | object[] | null = null

  const generateHeaders = () => {
    const headers: any = {}

    headers.Authorization = `Bearer ${apiToken.value}`

    return headers
  }

  const send = async (
    method: 'GET' | 'POST' | 'PUT' | 'DELETE' | 'PATCH',
    url: string
  ) => {
    return await $api
      .send(url, {
        method: method,
        headers: generateHeaders(),
        params,
        body
      })
      .then((response) => response as Data)
      .catch((response: Error) => {
        throw new fetchApiErrorHandler(response)
      })
  }

  return {
    params(data: object | object[]) {
      params = data
      return this
    },
    body(data: object | object[]) {
      body = data
      return this
    },
    get(url: string) {
      return send('GET', url)
    },
    post(url: string) {
      return send('POST', url)
    },
    put(url: string) {
      return send('PUT', url)
    },
    delete(url: string) {
      return send('DELETE', url)
    },
    patch(url: string) {
      return send('PATCH', url)
    }
  }
}
