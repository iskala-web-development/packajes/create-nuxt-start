import type { NuxtApp, NuxtPayload } from 'nuxt/app'
import type {
  AsyncDataOptions,
  KeysOf,
  AsyncData,
  PickFrom,
  _AsyncData,
  AsyncDataExecuteOptions
} from '#app/composables/asyncData'
import type { NuxtError } from '#app/composables/error'
import type {
  DefaultAsyncDataErrorValue,
  DefaultAsyncDataValue
} from '#app/defaults'
import { fetchApiErrorHandler } from '../handlers/fetchApiHandler'
import errorValidate from './errorValidate'

interface AsyncDataOptionsFetch<
  ResT,
  DataT = ResT,
  PickKeys extends KeysOf<DataT> = KeysOf<DataT>,
  DefaultT = DefaultAsyncDataValue
> extends AsyncDataOptions<ResT, DataT, PickKeys, DefaultT> {
  cache?: boolean
  onResponseError?: (value: fetchApiErrorHandler) => void
  onResponseSuccess?: (value: any) => void
  onResponsePending?: (status: boolean) => void
  onRequest?: () => void
  beforeFetch?: () => Promise<boolean> | boolean
  validate?: () => void
}

export declare function DUseAsyncData<
  ResT,
  NuxtErrorDataT = unknown,
  DataT = ResT,
  PickKeys extends KeysOf<DataT> = KeysOf<DataT>,
  DefaultT = DefaultAsyncDataValue
>(
  handler: (ctx?: NuxtApp) => Promise<ResT>,
  options?: AsyncDataOptionsFetch<ResT, DataT, PickKeys, DefaultT>
): AsyncData<
  PickFrom<DataT, PickKeys> | DefaultT,
  | (NuxtErrorDataT extends Error | NuxtError
      ? NuxtErrorDataT
      : NuxtError<NuxtErrorDataT>)
  | DefaultAsyncDataErrorValue
>

export declare function DUseAsyncDataWithKey<
  ResT,
  NuxtErrorDataT = unknown,
  DataT = ResT,
  PickKeys extends KeysOf<DataT> = KeysOf<DataT>,
  DefaultT = DefaultAsyncDataValue
>(
  key: string,
  handler: (ctx?: NuxtApp) => Promise<ResT>,
  options?: AsyncDataOptionsFetch<ResT, DataT, PickKeys, DefaultT>
): AsyncData<
  PickFrom<DataT, PickKeys> | DefaultT,
  | (NuxtErrorDataT extends Error | NuxtError
      ? NuxtErrorDataT
      : NuxtError<NuxtErrorDataT>)
  | DefaultAsyncDataErrorValue
>

export const asyncData = async (
  ...args: Parameters<typeof DUseAsyncData | typeof DUseAsyncDataWithKey>
) => handle('useAsyncData', ...args)

export const lazyAsyncData = async (
  ...args: Parameters<typeof DUseAsyncData | typeof DUseAsyncDataWithKey>
) => handle('useLazyAsyncData', ...args)

const handle = async (
  name: 'useAsyncData' | 'useLazyAsyncData',
  ...args: Parameters<typeof DUseAsyncData | typeof DUseAsyncDataWithKey>
) => {
  let asyncData
  let options

  if (typeof args[0] === 'function') {
    options = args[1] = { ...args[1] }
  } else if (typeof args[1] === 'function') {
    options = args[2] = { ...args[2] }
  } else {
    throw createError('Ошибка в аргументах asyncData')
  }

  const getCacheData = options.getCachedData

  options.getCachedData = (key: string, nuxtApp: any) => {
    const cacheData = getCacheData ? getCacheData(key, nuxtApp) : undefined

    if (typeof cacheData !== 'undefined') return cacheData

    const data = nuxtApp.payload.data[key] || nuxtApp.static.data[key]

    if (options.cache && data) return data
  }

  if (name === 'useAsyncData') {
    // @ts-ignore
    asyncData = await useAsyncData(...args).then((response) =>
      convertAsyncData(
        response as _AsyncData<unknown, NuxtError<unknown> | null>
      )
    )
  } else {
    // @ts-ignore
    asyncData = await useLazyAsyncData(...args).then((response) =>
      convertAsyncData(
        response as _AsyncData<unknown, NuxtError<unknown> | null>
      )
    )
  }

  watch(asyncData.status, (value) => {
    if (value === 'pending' && options.onResponsePending) {
      options.onResponsePending(true)
    } else if (options.onResponsePending) {
      options.onResponsePending(false)
    }

    if (value === 'error' && asyncData.error.value && options.onResponseError)
      options.onResponseError(asyncData.error.value)

    if (value === 'success' && options.onResponseSuccess)
      options.onResponseSuccess(asyncData.data.value)
  })

  const isLoadingAsync = computed(() => {
    return (
      asyncData.status.value === 'pending' || asyncData.status.value === 'idle'
    )
  })

  const isLoading = computed(() => {
    return asyncData.status.value === 'pending'
  })

  const loadingAsync = computed(() => {
    return (
      asyncData.status.value === 'pending' || asyncData.status.value === 'idle'
    )
  })

  const loading = computed(() => {
    return asyncData.status.value === 'pending'
  })

  const execute = async (opts?: AsyncDataExecuteOptions) => {
    const beforeFetchResult = options.beforeFetch
      ? await options.beforeFetch()
      : undefined

    if (beforeFetchResult !== false) return asyncData.execute(opts)
  }

  const refresh = async (opts?: AsyncDataExecuteOptions) => {
    const beforeFetchResult = options.beforeFetch
      ? await options.beforeFetch()
      : undefined

    if (beforeFetchResult !== false) return asyncData.refresh(opts)
  }

  return {
    ...asyncData,
    execute,
    refresh,
    errorValidate,
    isLoadingAsync,
    isLoading,
    loading,
    loadingAsync
  }
}

const convertAsyncData = (
  response: _AsyncData<unknown, NuxtError<unknown> | null>
) => {
  const data = response.data as Ref<any>
  const error: Ref<fetchApiErrorHandler | undefined | null> = ref()

  if (error.value) {
    error.value = new fetchApiErrorHandler(
      response.error.value
    ) as fetchApiErrorHandler
  }

  watch(response.error, (value) => {
    if (value) {
      error.value = new fetchApiErrorHandler(value) as fetchApiErrorHandler
    } else {
      error.value = null
    }
  })

  return { ...response, data, error }
}
