interface validateMethods {
  validateRules?: (rules: any) => void
  validateError?: (code: number | undefined, message: string) => void
}

export default (
  error: IFetchApiErrorHandler | undefined | null,
  validateMethods: validateMethods
) => {
  if (error) {
    switch (error?.statusCode) {
      case 422:
        validateMethods.validateRules
          ? validateMethods.validateRules(error?.data?.fields)
          : null
        break
      default:
        validateMethods.validateError && !error.isAuto
          ? validateMethods.validateError(
              error?.statusCode,
              error?.data?.message ?? error.message
            )
          : null
    }
  }
}
