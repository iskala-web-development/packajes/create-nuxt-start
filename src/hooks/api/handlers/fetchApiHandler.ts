import type { H3Error } from 'h3'

export interface Data {
  status?: number
  success?: boolean
  data: any
  pagination?: {
    count: number
    total: number
    perPage: number
    currentPage: number
    totalPages: number
    links: object
  }
  error?: DataError
}

interface DataError extends Data {
  code?: string
  message?: string
  fields?: any[]
}

export interface IFetchApiErrorHandler<T = any> extends Error {
  cause: any
  data?: DataError

  errorCode?: string | undefined
  errorMessage?: string | undefined

  fatal: boolean
  statusCode: number
  statusMessage?: string
  unhandled: boolean
  status?: string
  message: string
  detail?: string

  stack?: string | undefined

  __nuxt_error?: boolean
}

export class fetchApiErrorHandler
  extends Error
  implements IFetchApiErrorHandler
{
  private statuses = {
    fetchError: 'fetch_error'
  }

  private messages = {
    fetchError: 'Сервер временно не доступен'
  }

  public override cause: any
  public data?: DataError

  public errorCode?: string | undefined
  public errorMessage?: string | undefined

  public fatal: boolean
  public statusCode: number
  public statusMessage?: string
  public unhandled: boolean

  public status?: string
  public override message: string
  public detail?: string

  override stack?: string | undefined

  public __nuxt_error?: boolean

  public toJSON(): Pick<
    H3Error<unknown>,
    'data' | 'statusCode' | 'statusMessage' | 'message'
  > {
    return {
      data: this.data,
      statusCode: this.statusCode,
      statusMessage: this.statusMessage,
      message: this.message
    }
  }

  //override cause: any

  public constructor(response: any, eventHandler: boolean = false) {
    super()

    this.cause = response?.cause
    this.data = this.convertDate(response?.data)
    this.fatal = response?.fatal
    this.statusCode = response?.statusCode
    this.statusMessage = response?.statusMessage
    this.unhandled = response?.unhandled
    this.status = response?.statusCode
    this.message = this.generateMessage()
    this.stack = response?.stack
    this.__nuxt_error = response?.__nuxt_error

    this.generateError()
    this.checkConnectionTimeout()
    this.checkAuthError()

    if (eventHandler) this.eventsHandlerError()
  }

  private convertDate(data: any) {
    return data?.error ? data?.error : data
  }

  private generateMessage() {
    return this.data?.message ?? 'Критическая ошибка сервера'
  }

  private generateError() {
    this.errorCode = this.data?.code
    this.errorMessage = this.data?.message
  }

  private checkConnectionTimeout() {
    if (this.statusCode === undefined || this.statusCode === 504) {
      this.statusCode = 504
      this.message = this.messages.fetchError
    }
  }

  private checkAuthError() {}

  private eventsHandlerError() {}
  // private events() {
  //   const { $event } = useNuxtApp()

  //   if (!this.auto) return false

  //   $event.emit('toast:error', {
  //     message: this.message,
  //     detail: this.detail
  //   })
  // }
}

export class fetchApiSuccessHandler {
  public data?: any

  public constructor(response: any) {
    this.data = response?.data ?? null
  }
}
