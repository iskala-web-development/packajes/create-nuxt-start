interface IError extends Error {
  data?: any
  status?: number
  statusCode?: number
  statusMessage?: string
  statusText?: string
}

interface Data {
  status?: number
  success?: boolean
  data: any
  error?: DataError
}

interface DataError extends Data {
  code?: string
  message?: string
}

export interface IErrorHandler<T = any> extends Error {
  message: string

  data?: DataError

  errorCode?: string | undefined
  errorMessage?: string | undefined

  statusCode?: number
  statusMessage?: string
}

export default class ErrorHandler
  extends Error
  implements IErrorHandler, IError
{
  public data?: DataError

  public errorCode?: string | undefined
  public errorMessage?: string | undefined

  public status?: number | undefined
  public statusCode?: number | undefined

  public statusMessage?: string | undefined
  public statusText?: string | undefined

  //override cause: any

  public constructor({
    message,
    data,
    statusMessage,
    statusText,
    statusCode,
    status
  }: IError) {
    super(message)

    this.data = this.convertDate(data)
    this.statusMessage = statusMessage
    this.statusText = statusText
    this.statusCode = statusCode
    this.status = status

    this.generateError()
    this.checkConnectionTimeout()
  }

  private convertDate(data: any) {
    return data?.error ? data?.error : data
  }

  private generateError() {
    this.errorCode = this.data?.code
    this.errorMessage = this.data?.code
  }

  private checkConnectionTimeout() {
    if (!this.statusCode) {
      this.statusCode = 504
      this.message = 'Сервер временно не доступен'
    }
  }
}
