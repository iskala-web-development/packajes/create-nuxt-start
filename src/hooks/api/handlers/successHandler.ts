export interface Data {
  status?: number
  success?: boolean
  data: any
}

export interface ISuccessHandler {
  data?: Data
}

export default class SuccessHandler implements ISuccessHandler {
  public data?: Data

  //override cause: any

  public constructor(response: Data) {
    this.data = this.convertDate(response)
  }

  private convertDate(response: Data) {
    return response?.data
  }
}
