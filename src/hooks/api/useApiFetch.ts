import type { UseFetchOptions } from 'nuxt/app'
import type { FetchResult } from '#app/composables/fetch'
import type { NitroFetchRequest, AvailableRouterMethod } from 'nitropack'
import { fetchApiErrorHandler } from './handlers/fetchApiHandler'
// import useValidateHandler from './libs/useValidateHandler'

export default () => {
  const { $api } = useNuxtApp()

  let cache = false
  let params: object | object[] = {}
  let body: object | object[] | null = null
  let lazy = false

  const send = async <
    ResT = void,
    ReqT extends NitroFetchRequest = NitroFetchRequest,
    Method extends AvailableRouterMethod<ReqT> = ResT extends void
      ? 'get' extends AvailableRouterMethod<ReqT>
        ? 'get'
        : AvailableRouterMethod<ReqT>
      : AvailableRouterMethod<ReqT>,
    _ResT = ResT extends void ? FetchResult<ReqT, Method> : ResT
  >(
    method: 'GET' | 'POST' | 'PUT' | 'DELETE' = 'GET',
    url: string,
    options: UseFetchOptions<any> = {}
  ) => {
    const getCacheData = options.getCachedData

    options.getCachedData = (key: string, nuxtApp: any) => {
      const cacheData = getCacheData ? getCacheData(key, nuxtApp) : undefined

      if (typeof cacheData !== 'undefined') return cacheData

      const data = nuxtApp.payload.data[key] || nuxtApp.static.data[key]

      if (cache && data) return data
    }

    const fetch = await useFetch(url, {
      method: method,
      query: params,
      body,
      lazy,
      $fetch: $api,
      ...options
    }).then((response) => {
      const data: Ref<any> = ref(null)
      const error: Ref<fetchApiErrorHandler | undefined | null> = ref()

      error.value = new fetchApiErrorHandler(
        response.error.value
      ) as fetchApiErrorHandler

      data.value = response.data.value?.data

      watch(response.error, (value) => {
        if (value)
          error.value = new fetchApiErrorHandler(value) as fetchApiErrorHandler
        else {
          error.value = null
        }
      })

      watch(response.data, (value) => {
        if (value) data.value = value?.data
        else {
          data.value = null
        }
      })

      return { ...response, error, data }
    })

    const isLoading = computed(() => {
      return fetch.status.value === 'pending' || fetch.status.value === 'idle'
    })

    return { ...fetch, isLoading }
  }

  return {
    cache() {
      cache = true
      return this
    },
    lazy() {
      lazy = true
      return this
    },
    params(data: object | object[]) {
      params = data
      return this
    },
    body(data: object | object[]) {
      body = data
      return this
    },
    async get(url: string, options: UseFetchOptions<any> = {}) {
      return await send('GET', url, options)
    },
    async post(url: string, options: UseFetchOptions<any> = {}) {
      return await send('POST', url, options)
    },
    async put(url: string, options: UseFetchOptions<any> = {}) {
      return await send('PUT', url, options)
    },
    async delete(url: string, options: UseFetchOptions<any> = {}) {
      return await send('DELETE', url, options)
    }
  }
}
