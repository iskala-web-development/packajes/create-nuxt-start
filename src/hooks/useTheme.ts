export default () => {
  const runtimeConfig = useRuntimeConfig()
  const colorMode: any = useColorMode()

  const staticTheme = runtimeConfig.public.staticTheme ?? false

  const theme: Ref<string> = useCookie('theme', {
    maxAge: 604800
  }) // Dark & Light & System

  const themeColor: Ref<string> = useCookie('themeColor', {
    maxAge: 604800
  }) // Dark & Light

  if (staticTheme) {
    theme.value = staticTheme
    colorMode.preference = staticTheme
    themeColor.value = staticTheme
  }

  const setLightTheme = () => (colorMode.preference = 'light')
  const setDarkTheme = () => (colorMode.preference = 'dark')
  const setSystemTheme = () => (colorMode.preference = 'system')

  watch(colorMode, () => {
    theme.value = colorMode.preference
    themeColor.value = colorMode.value
  })

  return { theme, themeColor, setLightTheme, setDarkTheme, setSystemTheme }
}
