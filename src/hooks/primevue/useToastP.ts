import { useToast as useToastPrimevue } from 'primevue/usetoast'

export default () => {
  const toast = useToastPrimevue()

  let life: number | undefined = 3000

  const show = (
    message: string,
    type: 'success' | 'info' | 'warn' | 'error' | 'secondary' | 'contrast',
    detail: string | undefined = undefined
  ) => {
    toast.add({ severity: type, summary: message, detail, life })
  }

  return {
    show,
    setInfinityLife() {
      life = undefined // Равно бесконечность
      return this
    },
    setLife(value: number) {
      life = value
      return this
    },
    success(message: string, detail: string | undefined = undefined) {
      return show(message, 'success', detail)
    },
    info(message: string, detail: string | undefined = undefined) {
      return show(message, 'info', detail)
    },
    warn(message: string, detail: string | undefined = undefined) {
      return show(message, 'warn', detail)
    },
    error(message: string, detail: string | undefined = undefined) {
      return show(message, 'error', detail)
    },
    secondary(message: string, detail: string | undefined = undefined) {
      return show(message, 'secondary', detail)
    },
    contrast(message: string, detail: string | undefined = undefined) {
      return show(message, 'contrast', detail)
    }
  }
}
