export default () => {
  const props = defineProps({
    modelValue: {
      type: String,
      required: false,
      default: ''
    },
    label: {
      type: String,
      required: false
    },
    name: {
      type: String,
      required: true
    },
    rules: {
      type: String,
      required: false,
      default: ''
    },
    type: {
      type: String,
      required: false
    },
    small: {
      type: String,
      required: false
    },
    float: {
      type: Boolean,
      required: false
    }
  })

  return { props }
}
