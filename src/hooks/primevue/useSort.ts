import type { Reactive } from 'vue'

export default () => {
  const sort = async (sort: any, params: Reactive<any>) => {
    params.sort_key = sort.sortField ?? 'id'
    params.sort_order =
      sort.sortOrder === 1 || sort.sortOrder === null ? 'asc' : 'desc'
  }

  return { sort }
}
