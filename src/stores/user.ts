import type { Reactive } from 'vue'
import type { Paginate } from '~/types/paginate'

export enum UserRole {
  ADMIN = 'admin',
  EMPLOYEE = 'employee'
}

export interface User {
  id: number
  email: string
  role: UserRole
  verify_at: string | null
  created_at: string
  updated_at: string
}

export const useUserStore = defineStore(
  'user',
  () => {
    const apiToken = useCookie('api_token', { maxAge: 86400 })
    const user: Ref<User | null> = ref(null)

    const roles = [
      {
        name: 'Администратор',
        value: 'admin'
      },
      {
        name: 'Сотрудник',
        value: 'employee'
      }
    ]

    const clearAction = () => {
      user.value = null
      apiToken.value = null
    }

    const hasRoles = (roles: UserRole[]) => {
      return user.value ? roles.includes(user.value.role) : false
    }

    const getRoleName = (userRole: UserRole) => {
      return roles.find((role) => role.value === userRole)?.name
    }

    const me = async () => {
      return await useApi()
        .get('user/me')
        .then(({ data }) => (user.value = data && data))
    }

    const login = async (email: string, password: string) => {
      return await useApi()
        .body({
          email,
          password,
          system: 'web'
        })
        .post('auth/login')
        .then(({ data }) => {
          user.value = data.user
          apiToken.value = data.api_token

          return data
        })
    }

    const register = async (
      email: string,
      password: string,
      passwordConfirmation: string
    ) => {
      return await useApi()
        .body({
          email,
          password,
          password_confirmation: passwordConfirmation,
          system: 'web'
        })
        .post('auth/register')
        .then((response) => {
          user.value = response.data.user
          apiToken.value = response.data.api_token

          return response
        })
    }

    const getAll = async (params: Reactive<Paginate>) => {
      return await useApi().params(params).get('users')
    }

    const get = async (userId: number) => {
      return await useApi().get(`user/${userId}`)
    }

    const create = async (email: string, password: string, role: UserRole) => {
      return await useApi()
        .body({
          email,
          password,
          role
        })
        .post('user')
    }

    const update = async (
      id: number,
      email: string,
      password: string | undefined,
      role: UserRole
    ) => {
      return await useApi()
        .body({
          email,
          password,
          role
        })
        .put(`user/${id}`)
    }

    const destroy = async (id: number) => {
      return await useApi().delete(`user/${id}`)
    }

    return {
      // States
      user,
      apiToken,
      roles,

      // Methods
      clearAction,
      hasRoles,
      getRoleName,

      // Actions Requests
      me,
      login,
      register,
      getAll,
      get,
      create,
      update,
      destroy
    }
  },
  {
    persist: {
      paths: ['user']
    }
  }
)
