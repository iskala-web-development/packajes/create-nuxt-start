/**
 * * Development global function and variables
 **/

import event from './libs/event'
import declension from './libs/declension'

export default {
  event,
  declension
}
