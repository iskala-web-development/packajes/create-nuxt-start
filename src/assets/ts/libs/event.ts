import mitt from 'mitt'

const emitter = mitt<any>()

export default {
  emit: emitter.emit, // Will emit an event
  on: emitter.on // Will register a listener for an event
}
