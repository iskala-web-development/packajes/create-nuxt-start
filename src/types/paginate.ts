export interface Paginate {
  page: number
  limit: number
  sort_key: string | null
  sort_order: string | null
}
