import { useUserStore } from '~/stores/user'

export default defineNuxtRouteMiddleware(async (to, from) => {
  const userStore = useUserStore()
  const { user, apiToken } = storeToRefs(userStore)

  if (apiToken.value && !user.value) {
    await useAsyncDataFetch(() => userStore.me(), {
      onResponseError(error) {
        userStore.clearAction()
        abortNavigation()
        return navigateTo('/auth/login')
      }
    })
  } else if (!apiToken.value) {
    userStore.clearAction()
    abortNavigation()
    return navigateTo('/auth/login')
  }

  return true
})
