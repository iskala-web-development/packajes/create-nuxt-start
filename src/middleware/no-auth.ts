import { useUserStore } from '~/stores/user'

export default defineNuxtRouteMiddleware(async (to, from) => {
  const userStore = useUserStore()
  const { apiToken } = storeToRefs(userStore)

  if (apiToken.value) {
    abortNavigation()
    return navigateTo('/')
  }

  return true
})
