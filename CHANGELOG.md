# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.0.25](https://gitlab.com/iskala-web-development/packajes/create-nuxt-start/compare/v1.0.24...v1.0.25) (2024-10-27)

### [1.0.24](https://gitlab.com/iskala-web-development/packajes/create-nuxt-start/compare/v1.0.23...v1.0.24) (2024-10-09)

### [1.0.23](https://gitlab.com/iskala-web-development/packajes/create-nuxt-start/compare/v1.0.22...v1.0.23) (2024-09-13)

### [1.0.22](https://gitlab.com/iskala-web-development/packajes/create-nuxt-start/compare/v1.0.21...v1.0.22) (2024-09-13)

### 1.0.21 (2024-09-13)

### [1.0.20](https://gitlab.com/iskala-web-development/packajes/create-nuxt-start/compare/v1.0.19...v1.0.20) (2024-08-05)

### [1.0.19](https://gitlab.com/iskala-web-development/packajes/create-nuxt-start/compare/v1.0.18...v1.0.19) (2024-08-05)

### [1.0.18](https://gitlab.com/iskala-web-development/packajes/create-nuxt-start/compare/v1.0.17...v1.0.18) (2024-08-05)

### [1.0.17](https://gitlab.com/iskala-web-development/packajes/create-nuxt-start/compare/v1.0.16...v1.0.17) (2024-07-26)

### [1.0.16](https://gitlab.com/iskala-web-development/packajes/create-nuxt-start/compare/v1.0.15...v1.0.16) (2024-07-17)

### [1.0.15](https://gitlab.com/iskala-web-development/packajes/create-nuxt-start/compare/v1.0.14...v1.0.15) (2024-07-17)

### [1.0.14](https://gitlab.com/iskala-web-development/packajes/create-nuxt-start/compare/v1.0.13...v1.0.14) (2024-07-17)

### [1.0.13](https://gitlab.com/iskala-web-development/packajes/create-nuxt-start/compare/v1.0.12...v1.0.13) (2024-06-03)

### [1.0.12](https://gitlab.com/iskala-web-development/packajes/create-nuxt-start/compare/v1.0.11...v1.0.12) (2024-04-25)

### [1.0.11](https://gitlab.com/iskala-web-development/packajes/create-nuxt-start/compare/v1.0.10...v1.0.11) (2024-04-25)

### [1.0.10](https://gitlab.com/iskala-web-development/packajes/create-nuxt-start/compare/v1.0.9...v1.0.10) (2024-04-25)

### 1.0.9 (2024-04-25)

### [1.0.8](https://gitlab.com/iskala-web-development/packajes/create-nuxt-start/compare/v1.0.7...v1.0.8) (2024-03-31)

### [1.0.7](https://gitlab.com/iskala-web-development/packajes/create-nuxt-start/compare/v1.0.6...v1.0.7) (2024-03-31)

### 1.0.6 (2024-03-31)
