export default [
  {
    root: true,
    env: {
      browser: true,
      node: true
    },
    extends: [
      'eslint:recommended',
      'plugin:vue/vue3-recommended',
      'plugin:@typescript-eslint/recommended',
      '@nuxtjs/eslint-config-typescript',
      'prettier'
    ],
    plugins: ['vue', '@typescript-eslint'],
    rules: {
      // override/add rules settings here, such as:
      // 'vue/no-unused-vars': 'error'
      'vue/multi-word-component-names': 'off'
    }
  }
]
