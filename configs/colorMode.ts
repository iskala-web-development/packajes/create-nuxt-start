const preference = process.env.STATIC_THEME ?? 'system'

export default {
  preference,
  classSuffix: '-mode'
}
