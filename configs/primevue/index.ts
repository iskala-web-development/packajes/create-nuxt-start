import noir from './themes/noir'
import locale from './locale'

export default {
  // usePrimeVue: true,
  // autoImport: true,
  options: {
    ripple: true,
    theme: {
      preset: noir,
      options: {
        prefix: '',
        darkModeSelector: '.dark-mode'
      }
    },
    locale
  }
}
