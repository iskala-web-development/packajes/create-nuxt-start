import indigo from './indigo'
import noir from './noir'
import sky from './sky'

export default { noir, sky, indigo }
