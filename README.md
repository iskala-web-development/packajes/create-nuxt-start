# Nuxt 3 Minimal Starter

Look at the [Nuxt 3 documentation](https://nuxt.com/docs/getting-started/introduction) to learn more.

## Setup With Docker

Make sure to install the dependencies:

```bash
# Dev or Prod
docker build -t frontend --target dev .

# Run docker container (Add -d flag for no console watcher)
docker run -p 3000:3000 -v $(pwd):/var/www --name frontend frontend
```

## Setup Development

Make sure to install the dependencies:

```bash
bun install
bun run dev
```

## Setup Production

Build the application for production:

```bash
bun run build
bun run preview
```
> Использовать pm2 или dumb-init для docker

Check out the [deployment documentation](https://nuxt.com/docs/getting-started/deployment) for more information.
