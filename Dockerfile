FROM node:20-alpine AS base

# Set variable
ENV APP_ROOT '/var/www'

# Install dependencies servers
RUN apk --update --no-cache add ca-certificates \
  unzip \
  curl \
  wget

RUN wget -q -O /etc/apk/keys/sgerrand.rsa.pub https://alpine-pkgs.sgerrand.com/sgerrand.rsa.pub
RUN wget https://github.com/sgerrand/alpine-pkg-glibc/releases/download/2.28-r0/glibc-2.28-r0.apk
RUN apk add --no-cache --force-overwrite glibc-2.28-r0.apk

# Install bun
RUN npm install -g bun

# Set working directory
WORKDIR ${APP_ROOT}

# Copy existing application directory contents
ADD . ${APP_ROOT}

RUN chmod -R 777 docker-entrypoint.sh

FROM base as dev
# Set variable
ENV NODE_ENV development
ENV CHOKIDAR_USEPOLLING=true

ENTRYPOINT ["sh", "docker-entrypoint.sh"]
CMD ["bun", "run", "dev"]

FROM base as prod
# Set variable
ENV NODE_ENV production

RUN apk --update --no-cache add dumb-init

ENTRYPOINT ["sh", "docker-entrypoint.sh"]
CMD [ "dumb-init", "node", ".output/server/index.mjs" ]
