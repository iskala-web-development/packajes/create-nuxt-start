// @ts-ignore
import eslintPlugin from 'vite-plugin-eslint'
import googleFonts from './configs/google-fonts.json'
import veeValidate from './configs/vee-validate.json'
import tailwindcss from './configs/tailwindcss.json'
import primevue from './configs/primevue'
import colorMode from './configs/colorMode'
import pinia from './configs/pinia.json'

// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  css: ['@/assets/scss/app.scss'],

  // При простой верстке - отключить
  ssr: true,

  typescript: {
    strict: true
  },

  devtools: { enabled: true },
  srcDir: 'src',

  imports: {
    dirs: ['hooks/**']
  },

  runtimeConfig: {
    public: {
      baseUrlApi: process.env.BASE_URL_API,
      baseUrlVersion: process.env.BASE_URL_VERSION,
      baseUrlPrefix: process.env.BASE_URL_PREFIX,
      staticTheme: process.env.STATIC_THEME
    }
  },

  vite: {
    plugins: [
      eslintPlugin()
      // stylelintPlugin()
    ],
    css: {
      preprocessorOptions: {
        scss: {
          additionalData:
            '@use "sass:math"; @import "@/assets/scss/settings.scss";'
        }
      }
    }
  },

  modules: [
    /**
     * @name Pinia
     * @url https://pinia.vuejs.org/ssr/nuxt.html
     */
    '@pinia/nuxt',

    /**
     * @name PiniaPersisted - Плагин позволяет устанавливать статические сохраняемые данные
     * @url https://prazdevs.github.io/pinia-plugin-persistedstate/frameworks/nuxt-3.html
     */
    '@pinia-plugin-persistedstate/nuxt', // https://vueuse.org/guide/

    /**
     * @name Vueuse - Набор функций для vue, nuxt
     * @url https://vueuse.org/guide/
     */
    '@vueuse/nuxt', // https://image.nuxt.com/

    /**
     * @name NuxtImage - Работа с картинками Nuxt
     * @url https://image.nuxt.com/
     */
    '@nuxt/image', // https://color-mode.nuxtjs.org/

    /**
     * @name СolorMode - Работа с темами и цветами в Nuxt
     * @url https://color-mode.nuxtjs.org/
     */
    '@nuxtjs/color-mode',

    /**
     * @name GoogleFonts - Автоматическая установка шрифтов из Google в Nuxt
     * @url https://google-fonts.nuxtjs.org/getting-started/setup
     */
    '@nuxtjs/google-fonts',

    /**
     * @name VeeValidate - Работа с валидацией в Nuxt (Позволяет валидировать данные как в Laravel)
     * @url https://nuxt.com/modules/vee-validate
     */
    '@vee-validate/nuxt',

    /**
     * @name TypedRouter - Помогает создавать имеющейся роуты (pages) и при их отсутствие выдавать предупреждение в Nuxt
     * @url https://nuxt.com/modules/typed-routere
     */
    'nuxt-typed-router',

    /**
     * @name DayJs - Удобная работа с конвертацией даты и времени в Nuxt
     * @url https://nuxt.com/modules/dayjs
     */
    'dayjs-nuxt',

    /**
     * @name Icons - Позволяет создавать иконки в папке assets/icons (Типо iconmoon) в Nuxt
     * @url https://nuxt.com/modules/icons
     */
    'nuxt-icons',

    /**
     * @name Tailwind - Tailwind в Nuxt
     * @url // https://tailwindcss.nuxtjs.org/getting-started/installation
     */
    '@nuxtjs/tailwindcss',

    // PrimeVue 4
    /**
     * @name PrimeVue - Библиотека для верстки и логики Vue/Nuxt
     * @url // https://primevue.org/nuxt
     */
    '@primevue/nuxt-module',

    /**
     * @name Lodash - Библиотека набора полезных функций Vue/Nuxt
     * @url // https://nuxt.com/modules/lodash
     */
    'nuxt-lodash'
  ],

  plugins: ['~/plugins/vee-validation/index.ts'],

  // @ts-ignore
  googleFonts,

  veeValidate,
  tailwindcss,
  primevue,
  colorMode,
  pinia,

  // Отключаем сбор данных
  telemetry: false,

  compatibilityDate: '2024-07-16'
})
